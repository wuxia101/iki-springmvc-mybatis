package com.matosiki.mybatis.mapper;


import com.matosiki.mybatis.pojo.Order;
import com.matosiki.mybatis.pojo.User;
import com.matosiki.mybatis.util.Page;
import org.apache.ibatis.annotations.Param;

import java.util.List;


public interface UserMapper {
    public User getUserById(int userId);

    public List<Order> getUserOrders(int userId);

    public List<Order> getOrderListPage(@Param("page") Page page, @Param("userid") int userid);

    void insertUser(User user);
}
