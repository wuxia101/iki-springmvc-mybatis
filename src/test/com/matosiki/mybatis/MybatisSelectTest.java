package com.matosiki.mybatis;

import com.matosiki.mybatis.mapper.GroupMapper;
import com.matosiki.mybatis.mapper.UserGroupMapper;
import com.matosiki.mybatis.mapper.UserMapper;
import com.matosiki.mybatis.pojo.Group;
import com.matosiki.mybatis.pojo.Post;
import com.matosiki.mybatis.pojo.User;
import com.matosiki.mybatis.pojo.UserGroup;
import org.apache.ibatis.io.Resources;
import org.apache.ibatis.session.SqlSession;
import org.apache.ibatis.session.SqlSessionFactory;
import org.apache.ibatis.session.SqlSessionFactoryBuilder;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import java.io.IOException;
import java.io.Reader;
import java.util.List;


/**
 * Created by matosiki on 2018/1/30.
 */
public class MybatisSelectTest {
    private  SqlSessionFactory sqlSessionFactory;
    private  Reader reader;
    private SqlSession session;

    @Before
    public void setup() throws IOException {
        reader = Resources.getResourceAsReader("Configuration-test.xml");
        sqlSessionFactory =new SqlSessionFactoryBuilder().build(reader);
        session = sqlSessionFactory.openSession();
    }

    /**
     * one2many
     */
    @Test
    public void testSelectUserAndPosts(){
        int userid = 1;
        User user = session.selectOne("com.matosiki.mybatis.mapper.UserMapper.getUserAndPost", 1);
        System.out.println("username: " + user.getUsername() + ",");
        List<Post> posts = user.getPosts();
        for (Post p : posts) {
            System.out.println("Title:" + p.getTitle());
            System.out.println("Content:" + p.getContent());
        }

    }

    /**
     *  many2one
     */

    @Test
    public void testSelectPostsAndUser() {
        int postId = 1;
        Post post = session.selectOne("com.matosiki.mybatis.mapper.UserMapper.getPostsAndUser", postId);
        System.out.println("title: " + post.getTitle());
        System.out.println("userName: " + post.getUser().getUsername());
    }


    @Test
    public void testGetGroupAndUsers(){
        UserGroup userGroup = new UserGroup();
        GroupMapper groupMaper = session.getMapper(GroupMapper.class);
        Group group = groupMaper.getGroup(1);
        System.out.println("Group => " + group.getGroupName());
        List<User> users = group.getUsers();
        for (User user : users) {
            System.out.println("\t:" + user.getId() + "\t"
                    + user.getUsername());
        }
    }

    @Test
    public void testAddUserGroup(){
        UserGroup userGroup = new UserGroup();
        userGroup.setGroupId(1);
        userGroup.setUserId(2);
        UserGroupMapper userGroupMaper = session
                .getMapper(UserGroupMapper.class);
        userGroupMaper.insertUserGroup(userGroup);

        session.commit();
    }

    @Test
    public void testAddUser(){
        User user = new User();
        user.setUsername("User-name-1");
        user.setMobile("13838009988");
        UserMapper userMaper = session.getMapper(UserMapper.class);
        userMaper.insertUser(user);
        session.commit();
    }

    @Test
    public void testAddGroup(){
        Group group = new Group();
        group.setGroupName("用户组-1");
        GroupMapper groupMapper = session.getMapper(GroupMapper.class);
        groupMapper.insertGroup(group);
        session.commit();
        System.out.println(group.getGroupId());
    }
    @After
    public void after() {
        if(session != null){
            session.close();
        }
    }

}
