package com.matosiki.mybatis.mapper;

import com.matosiki.mybatis.pojo.User;
import com.matosiki.mybatis.pojo.UserGroup;

/**
 * Created by matosiki on 2018/1/31.
 */
public interface UserGroupMapper {
    public void insertUserGroup(UserGroup userGroup);
    public User getUsersByGroupId(int groupId);
    public  User getGroupsByUserId(int id);

}
