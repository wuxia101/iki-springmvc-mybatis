package com.matosiki.mybatis.mapper;

import com.matosiki.mybatis.pojo.Group;

/**
 * Created by matosiki on 2018/1/31.
 */
public interface GroupMapper {
    Group getGroup(int i);
    void insertGroup(Group group);
}
